---
layout: handbook-page-toc
title: "Twitch for Evangelists"
description: "Using streaming as a method of outreach for the Education Community"

---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

# Twitch

## Why

Twitch is being used as a method of outreach for the Education team to reach the broader community and create a catalogue of presentations, shows, and resources for people looking to learn GitLab or increase their knowledge of the program. The following sections will detail common use cases as well as explaining best practices for Twitch here in the Education Program. 

## Apps and hardware

### Apps

1. [OBS](https://obsproject.com/) or [Streamlabs OBS](https://streamlabs.com/)
    >These apps are designed to create a stream that is them sent to Twitch. We suggest Streamlabs OBS (sometimes abbreviated to SLOBS) because it is more customizable for a unique viewer experience, although you can achieve similar results with OBS. For experienced steamers, use whatever setup you prefer. 
1. [Blackhole](https://existential.audio/blackhole/)
    >OBS and SLOBS are unable to access Mac output audio natively. This 3rd party app allows for the output sound to be captured. If you don't plan to use any sounds other than your microphone, ,you won't need this, but a better viewer experience is achieved with desktop sounds including music, sound effects, and guest audio. 
1. [Restream](https://app.restream.io/home)
    >Restream is a web app studio where you can host guests. You're also able to stream directly to a variety of targets from it. Restream is currently used as the method for streaming with guests. Instructions can be found in [setting up a stream](FIXME WIP URL).

### Hardware
1. Good headphones are paramount to good quality audio. It's important to put the audio being listened to, or guests 
    >audio directly into your ears rather than into your room to avoid poor sound. 
1.  An external microphone and camera are an important part of a professional stream.  
    >(All of these are items you can [expense](https://about.gitlab.com/handbook/finance/expenses/) as part of working at GitLab. )
1. [Stream Deck](https://www.elgato.com/en/stream-deck)
    >A Stream Deck allows you to have even more control over your stream. You can change scenes, control music and sound effects, there's even a plug in for common commands in VSCode. A stream deck is not required, but is part of many common streaming set ups. 

## Guide

Using Twitch as part of the Education program involves using an Education Evangelists' personal Twitch account. You can find help creating one [here](https://help.twitch.tv/s/article/creating-an-account-with-twitch?language=en_US). Make sure to use your personal email and not your GitLab email so you don't lose access in case of changing jobs. 

Once created, follow these guides to create a streaming solution. 

* [Streamlabs OBS and Twitch](https://visualsbyimpulse.com/blog/a-beginners-guide-to-streaming-with-streamlabs-obs/)
* [Setting up Blackhole for SLOBS](https://streamlabs.com/content-hub/post/capturing-desktop-audio-in-streamlabs-obs-for-mac)